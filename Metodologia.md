# Metodologia Tradicional e metodologia ágil

## Metodologia Tradicional

A metodologia tradicional tem etapas bem definidas com uma abordagem em fases sequenciais em um modelo de cascata, em que uma etapa só começa depois que a etapa anterior foi concluída. O método tradicional deixa pouco espaço para mudança na visão original do projeto, possuindo uma divisão mais formal e burocrática em seu processo.

## Metodologia ágil

A metodologia ágil tem esse nome pois prioriza a entrega do projeto, quebrando o processo em partes para oferecer um produto aos clientes de maneira mais rápida e que permita um gerenciamento melhor das mudanças que ocorrem durante o processo, adotando uma postura de desenvolvimento incremental e iterativa.
